# waitcell
This crate provides the `WaitCell` type, which is a thread-safe cell which can only be written
once while shared. Attempts to read the value before it is written cause the reading thread to
block.

Because the value can be written to only once while shared, `WaitCell` provides access to
its value by shared reference.

# Documentation
Inline rustdoc documentation is available. A mirror of this documentation is
available at <https://docs.rs/waitcell>.

# Contributing
`waitcell` is developed at [GitLab].

Reasonable performance, correctness, documentation, and ease-of-use
contributions are always welcome. [Bug reports][Issues] and feature suggestions
are accepted through GitLab.

## Pull Requests
Please ensure pull requests match the existing coding style and are formatted
with rustfmt.

Spelling and grammatical errors are considered bugs, so please use
spell-checking facilities prior to submitting a contribution.

## Contribution Agreement
By contributing, you grant all contributors a perpetual, worldwide,
non-exclusive, no-charge, royalty-free, irrevocable copyright license to
reproduce, prepare derivative works of, publicly display, publicly perform,
relicense, sublicense, and distribute your contributions.

Additionally, you affirm that you are legally entitled to grant such license and
that your contributions are not and will not become patent-encumbered. In the
event that you discover that such affirmation was made in error, you agree to
post notice of such error in a conspicuous place (such as a
[GitLab Issue][Issues]) within three days.

# License
`waitcell` is licensed under the terms of the
[Apache License, Version 2.0][Apache2] or the [MIT License][MIT].

The corresponding [SPDX] license identifier is `Apache-2.0 OR MIT`.

# Copyright
This document is Copyright (C) 2020 Nathan Sharp.

Permission is granted to reproduce this document, in any form, free of charge.

[Apache2]: https://www.apache.org/licenses/LICENSE-2.0
[GitLab]: https://gitlab.com/nwsharp/waitcell
[Issues]: https://gitlab.com/nwsharp/waitcell/-/issues
[MIT]: https://opensource.org/licenses/MIT
[SPDX]: https://spdx.org/licenses
